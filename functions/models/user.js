'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ aadhar }) {
      this.hasOne(aadhar, { foreignKey: 'id' })
    }
  };
  user.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    mobile: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    gender: {
      type: DataTypes.ENUM,
      allowNull: false,
      values: ['Male', 'Female', 'I prefer not to say']
    },
    country: {
      type: DataTypes.ENUM,
      allowNull: false,
      values: ['India', 'Germany', 'UAE']
    }
  },
  {
    sequelize,
    modelName: 'user',
  });
  return user;
};