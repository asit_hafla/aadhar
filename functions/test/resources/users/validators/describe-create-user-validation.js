const { expect } = require("chai");
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const CreateUserValidator = require("resources/users/validators/create-user-validator");

describe("create user validation", async () => {
  it("should fail if user name is empty", async () => {
    let response = await CreateUserValidator.validate({
      name: '',
      mobile: '1234567890',
      email: 'user@example.com',
      password: '12345',
      gender: 'Male',
      country: 'India'
    });
    verifyResultError((error) => {
        expect(error.errorMessage).to.include('Name should not be empty.');
    })(response);
  });

  it("should fail if user mobile is empty", async () => {
    let response = await CreateUserValidator.validate({
      name: 'Test 1',
      mobile: '',
      email: 'user@example.com',
      password: '12345',
      gender: 'Male',
      country: 'India'
    });
    verifyResultError((error) => {
        expect(error.errorMessage).to.include('Mobile number should not be empty.');
    })(response);
  });

  it("should fail if user mobile is invalid", async () => {
    let response = await CreateUserValidator.validate({
      name: 'Test 1',
      mobile: '00',
      email: 'user@example.com',
      password: '12345',
      gender: 'Male',
      country: 'India'
    });
    verifyResultError((error) => {
        expect(error.errorMessage).to.include('Mobile number is not valid.');
    })(response);
  });

  it("should fail if user email is empty", async () => {
    let response = await CreateUserValidator.validate({
      name: 'Test 1',
      mobile: '1234567890',
      email: '',
      password: '12345',
      gender: 'Male',
      country: 'India'
    });
    verifyResultError((error) => {
        expect(error.errorMessage).to.include('Email should not be empty.');
    })(response);
  });

  it("should fail if user email is invalid", async () => {
    let response = await CreateUserValidator.validate({
      name: 'Test 1',
      mobile: '1234567890',
      email: 'fake-email',
      password: '12345',
      gender: 'Male',
      country: 'India'
    });
    verifyResultError((error) => {
        expect(error.errorMessage).to.include('Email is not valid.');
    })(response);
  });
});
