const chai = require(`chai`);
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require('helpers/verifiers');
const DeleteUserValidation = require('resources/users/validators/delete-user-validator');

describe('delete user validation', () => {
    it('should fail if user id is invalid', async () => {
		let response = await DeleteUserValidation.validate({userId: ''})
		verifyResultError(
		(error) => {
			expect(error.errorMessage).to.include('User id should not be empty.');
		}
		)(response)
	});

    it('Should be a Valid User Id', async () => {
		let response = await DeleteUserValidation.validate({userId: 'fake-id'})
        verifyResultError(
		(error) => {
			expect(error.errorMessage).to.include('User id is not valid.');
		}
		)(response)
	});

    it('Should be valid when All Data of User is provided', async () => {
        let response = await DeleteUserValidation.validate ({
            userId: '4eae15a7-306a-43e3-9098-18fdb3848c33'
        })
        verifyResultOk(() => { })(response)
    });

});
