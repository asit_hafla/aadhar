const { expect } = require("chai");
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const GetUserValidator = require("resources/users/validators/get-user-validator");

describe("get user validation", async () => {
  it("should fail if user is empty", async () => {
    let response = await GetUserValidator.validate({userId: ""});
    verifyResultError((error) => {
        expect(error.errorMessage).to.include('User id should not be empty.');
    })(response);
  });

  it("should fail if user is invalid", async () => {
    let response = await GetUserValidator.validate({userId: "fake-id"});
    verifyResultError((error) => {
        expect(error.errorMessage).to.include('User id is not valid.');
    })(response);
  });

  it("should pass if user id is valid", async () => {
      let response = await GetUserValidator.validate({userId: "4eae15a7-306a-43e3-9098-18fdb3848c33"});
      verifyResultOk(() => {})(response);
  });
});
