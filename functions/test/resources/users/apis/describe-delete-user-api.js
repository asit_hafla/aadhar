const Route = require('route');
const { logInfo, respond, whenResult } = require('lib');
const Result = require('folktale/result');
const Models = require('models');
const db = require('db/repository');
const DeleteUserQuery = require('resources/users/queries/delete-user-query.js');
const DeleteUserValidator = require('resources/users/validators/delete-user-validator');

async function deleteUser(req){
	const userId = req.params.id;

	const validationResult = await DeleteUserValidator.validate({userId});
	const user = await whenResult(
		() => {
			return db.execute(new DeleteUserQuery (userId));
		}
	)(validationResult)

	return respond(user, 'Successfully Deleted The User.','Failed to Delete The User');

}

Route.withOutSecurity().noAuth().delete('/users/:id',deleteUser).bind();
