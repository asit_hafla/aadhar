const ApiError = require('lib/functional/api-error')
const ValidationError = require('lib/validation-error')
const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const { expect } = chai;
const uuid = require('uuid')
const TestRoutes = require('helpers/test-route')
chai.use(sinonChai)
const db = require('db/repository')
const { resolveDbResult, resolveOk, resolveError, validationError } = require('helpers/resolvers')
const { verifyArgs } = require('helpers/verifiers')
const GetUserQuery = require('resources/users/queries/get-user-query')

describe('get users api', () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  beforeEach(() => {
    const id = uuid.v4();
    sandbox.stub(uuid, 'v4').returns(id);
    req = {
      params: {
        userId: id
      }
    }

    res = {
      id: id,
      name: 'name',
      email: 'email'
    }
  })

  it('should return user by id', async () => {
    sandbox.mock(db).expects('execute')
      .withArgs(verifyArgs((args) => {
        expect(args).to.be.instanceOf(GetUserQuery);
        expect(args).to.be.eql({ userId: req.params.userId });
      }))
      .returns(resolveOk(res));

    const response = await TestRoutes.execute('/users/:userId', 'Get', req);

    expect(response).to.be.eql({
        status: true,
        message: 'Successfully received user.',
        entity: res
    });
  });

  it('should return error if query failed', async () => {
    sandbox.stub(db, 'execute')
      .returns(resolveError('Query failed'));

    const response = await TestRoutes.executeWithError('/users/:userId', 'Get', req);
    expect(response).to.eql(new ApiError(0, 'Query failed', 'Failed to get user.'));
  })


  afterEach(() => {
    sandbox.verifyAndRestore();
  });
})