const ApiError = require('lib/functional/api-error')
const ValidationError = require('lib/validation-error')
const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const { expect } = chai;
const TestRoutes = require('helpers/test-route')
chai.use(sinonChai)
const uuid = require('uuid')
const db = require('db/repository')
const { resolveDbResult, resolveOk, resolveError, validationError } = require('helpers/resolvers')
const { verifyArgs } = require('helpers/verifiers')
const CreateUserQuery = require('resources/users/queries/create-user-query')
const CreateUserValidator = require('resources/users/validators/create-user-validator')

describe('create users api', () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  beforeEach(() => {
    id = uuid.v4();
    sandbox.stub(uuid, 'v4').returns(id);
    req = {
      body: {
        name: 'user1',
        mobile: '1234567890',
        email: 'user@example.com',
        password: '12345',
        gender: 'Male',
        country: 'India'
      }
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      })
    }
  })

  it('should create user when all the data are provided', async () => {
    sandbox.mock(db).expects('execute')
      .withArgs(verifyArgs((query) => {
        expect(query).to.be.instanceOf(CreateUserQuery)
        expect(query.details).to.be.eql({
          id,
          name: 'user1',
          mobile: '1234567890',
          email: 'user@example.com',
          password: '12345',
          gender: 'Male',
          country: 'India'
        })
      }))
      .returns(resolveOk({ id, name: 'user1', mobile: '1234567890', email: 'user@example.com', password: '12345', gender: 'Male', country: 'India' }))
    const response = await TestRoutes.execute('/users', 'Post', req, res);
    expect(response).to.be.eql({
      entity: { id, name: 'user1', mobile: '1234567890', email: 'user@example.com', password: '12345', gender: 'Male', country: 'India' },
      status: true,
      message: 'Successfully created a user.'
    })
  });

  it('respond failure when some error occurs', async () => {
    sandbox.mock(db).expects('execute').returns(resolveError('some error'));
    const response = await TestRoutes.executeWithError('/users', 'Post', req, res);
    expect(response).to.be.eql(new ApiError(0, 'some error', 'Failed to create user.'));
  });

  it('respond with validation error when incorrect data is provided', async () => {
    sandbox.mock(CreateUserValidator)
      .expects('validate')
      .returns(resolveError(new ValidationError(0, ['Name is mandatory'])));
    const response = await TestRoutes.executeWithError('/users', 'Post', req, res);
    expect(response).to.be.eql(new ValidationError(0, ['Name is mandatory']))

  })

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
})