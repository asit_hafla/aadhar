const chai = require('chai')
const sinon = require("sinon");
const { expect } = chai;
const db = require('db/repository')
const ds = require('helpers/dataSetup')
const { verifyArgs } = require("helpers/verifiers")
const { resolveOk, resolveError } = require("helpers/resolvers")
const ValidationError = require("lib/validation-error");
const TestRoute = require("helpers/test-route")
const RunQuery = require('data/run-query')
const GetUserValidator = require("resources/users/validators/get-user-validator");
const UpdateUserValidator = require("resources/users/validators/update-user-validator");
const UpdateUserQuery = require("resources/users/queries/update-user-query");

describe('update users query', () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  beforeEach(() => {
    req = {
      params: {
        userId: "4eae15a7-306a-43e3-9098-18fdb3848c33",
      },
      body: {
        name: "Test 2",
        email: 'test2@example.com',
      },
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  it("should update the user when user id correct", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .withArgs(
        verifyArgs((query) => {
          expect(query).to.be.instanceOf(UpdateUserQuery);
        })
      )
      .returns(resolveOk([0]));

    let response = await TestRoute.execute("/users/:userId", "Put", req, res);

    expect(response).to.be.eql({
      status: true,
      message: "Successfully updated a user.",
      entity: [0],
    });
  });

  it("should not update is data is not correct", async () => {
    const invalidReq = JSON.parse(JSON.stringify(req));
    invalidReq.params.userId = 'fake-id'
    sandbox
      .mock(UpdateUserValidator)
      .expects("validate")
      .returns(
        resolveError(new ValidationError(0, "User id is not valid."))
      );

    sandbox.mock(db).expects("execute").never();

    const response = await TestRoute.executeWithError(
      "/users/:userId",
      "Put",
      invalidReq,
      res
    );

    expect(response).to.be.eql(new ValidationError(0, "User id is not valid."))
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
})