const chai = require('chai')
const { expect } = chai;
const TestRoutes = require('helpers/test-route')
const { verifyResultOk, verifyResultError } = require('helpers/verifiers')
const uuid = require('uuid')
const db = require('db/repository')
const ds = require('helpers/dataSetup')
const RunQuery = require('data/run-query')
const CreateUserQuery = require('resources/users/queries/create-user-query');
// const user = require('../../../../models/user');

describe('create users query', () => {
  let user;
  beforeEach(async () => {
    user = await ds.buildSingle(ds.user);
  });

  it('should create a user', async () =>{
    const createdUserResponse = await db.execute(
      new CreateUserQuery(user.id, user.name, user.mobile, user.email, user.password, user.gender, user.country)
    )
    verifyResultOk(
      (createdUser) => {
        expect({
          id: createdUser.dataValues.id,
          name: createdUser.dataValues.name,
          mobile: createdUser.dataValues.mobile,
          email: createdUser.dataValues.email,
          password: createdUser.dataValues.password,
          gender: createdUser.dataValues.gender,
          country: createdUser.dataValues.country,
        }).to.be.eql({
          id: user.id,
          name: user.name,
          mobile: user.mobile,
          email: user.email,
          password: user.password,
          gender: user.gender,
          country: user.country
        });
      }
    )(createdUserResponse)
  });

  after(async () => {
    await ds.deleteAll();
  });
})