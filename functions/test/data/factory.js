const { factory } = require("factory-girl");

const loadFactory = async () => {
  factory.define("user", Object, {
    id: factory.chance("guid"),
    name: factory.chance("name"),
    mobile: factory.chance("phone", { mobile: true }),
    email: factory.chance("email"),
    password: factory.chance("string"),
    gender: factory.chance('gender'),
    country: 'India'
  });
};

module.exports.factory = factory;
module.exports.loadFactory = loadFactory;
