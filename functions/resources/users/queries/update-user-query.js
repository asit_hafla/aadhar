const models = require('models');

module.exports = class CreateUserQuery {
  constructor(userId, updatedUserDetails) {
    this.updatedUserDetails = updatedUserDetails;
    this.userId = userId
  }

  get() {
    return models.user.update(this.updatedUserDetails, { where: { id: this.userId }});
  }
}