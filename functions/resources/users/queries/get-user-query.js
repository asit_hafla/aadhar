const models = require('models');

module.exports = class GetUserQuery {
  constructor(id) {
    this.userId = id
  }

  get() {
    return models.user.findOne({
      where: {
        id: this.userId
      }
    });
  }
}