const models = require('models');

module.exports = class CreateUserQuery {
  constructor(userId) {
    this.userId = userId
  }

  get() {
    return models.user.destroy({ where: { id: this.userId }});
  }
}