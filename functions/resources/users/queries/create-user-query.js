const models = require('models');

module.exports = class CreateUserQuery {
  constructor(id, name, mobile, email, password, gender, country) {
    this.details = { id, name, mobile, email, password, gender, country }
  }

  get() {
    return models.user.create(this.details);
  }
}