const route = require('route');
const db = require('db/repository');
const { respond, logInfo, composeResult } = require('lib');
const UpdateUserQuery = require('../queries/update-user-query')
const UpdateUserValidator = require('../validators/update-user-validator')

const updateUser = async (req, res) => {
  const { userId } = req.params;
  const response = await composeResult(
    () => db.execute(new UpdateUserQuery(userId, {...req.body})),
    (userDetails) => UpdateUserValidator.validate(userDetails)
  )({userId})

  return respond(response, 'Successfully updated a user.', 'Failed to update user.')
}

route.withOutSecurity().noAuth().put('/users/:userId', updateUser).bind()