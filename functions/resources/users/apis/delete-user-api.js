const route = require('route');
const db = require('db/repository');
const { respond, logInfo, composeResult } = require('lib');
const DeleteUserQuery = require('../queries/delete-user-query')
const DeleteUserValidator = require('../validators/delete-user-validator')

const updateUser = async (req, res) => {
  const { userId } = req.params;
  const response = await composeResult(
    () => db.execute(new DeleteUserQuery(userId)),
    (userDetails) => DeleteUserValidator.validate(userDetails)
  )({userId})

  return respond(response, 'Successfully deleted a user.', 'Failed to delete user.')
}

route.withOutSecurity().noAuth().delete('/users/:userId', updateUser).bind()