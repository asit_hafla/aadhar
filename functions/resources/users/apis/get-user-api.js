const Route = require('route');
const db = require('db/repository');
const { composeResult, respond } = require('lib');
const { logInfo } = require('lib/functional/logger');
const GetUserQuery = require('resources/users/queries/get-user-query');
const GetUserValidator = require('resources/users/validators/get-user-validator')

const get = async (req) => {
    const {
        userId
    } = req.params;

    logInfo('Request to get user', {
        userId
    });

    const response = await composeResult(
        () => db.execute(new GetUserQuery(userId)),
        (userDetails) => GetUserValidator.validate(userDetails)
    )(req.params)

    return respond(response, 'Successfully received user.', 'Failed to get user.');
};

Route.withOutSecurity().noAuth().get('/users/:userId', get).bind();

module.exports = { get };
