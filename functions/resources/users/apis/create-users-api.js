const route = require('route');
const db = require('db/repository');
const uuid = require('uuid')
const { respond, logInfo, composeResult } = require('lib');
const CreateUserQuery = require('../queries/create-user-query')
const CreateUserValidator = require('../validators/create-user-validator')

const createUsers = async (req, res) => {
  const id = uuid.v4();
  const { name, mobile, email, password, gender, country } = req.body;
  const response = await composeResult(
    () => db.execute(new CreateUserQuery(id, name, mobile, email, password, gender, country)),
    (userDetails) => CreateUserValidator.validate(userDetails)
  )({id, name, mobile, email, password, gender, country})
  return respond(response, 'Successfully created a user.', 'Failed to create user.')
}

route.withOutSecurity().noAuth().post('/users', createUsers).bind()