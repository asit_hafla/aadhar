const {
  validate,
  notEmpty,
  shouldBeUuid,
  isEmail,
  isMobileNumber
} = require('validation');

const rule = {
  name: [
    [notEmpty, 'Name should not be empty.']
  ],
  mobile: [
    [notEmpty, 'Mobile number should not be empty.'],
    [isMobileNumber, 'Mobile number is not valid.']
  ],
  id: [
      [shouldBeUuid, 'Id is not valid.']
  ],
  email: [
    [notEmpty, 'Email should not be empty.'],
    [isEmail, 'Email is not valid.']
  ]
};

module.exports.validate = async data => validate(rule, data);
