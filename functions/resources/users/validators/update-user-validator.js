const {
  validate,
  notEmpty,
  shouldBeUuid,
} = require('validation');

const rule = {
  userId: [
    [notEmpty, 'User id should not be empty.'],
    [shouldBeUuid, 'User id is not valid.']
  ]
};

module.exports.validate = async data => validate(rule, data);
