const {
  validate,
  shouldBeUuid,
  notEmpty
} = require('validation');

const rule = {
  aadhar: [
    [notEmpty, 'Aadhar should not be empty!']
  ],
  userId: [
    [shouldBeUuid, 'userId should be valid!']
  ],
};

module.exports.validate = async data => validate(rule, data);