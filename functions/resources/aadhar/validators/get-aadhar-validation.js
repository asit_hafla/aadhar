const {
  validate,
  shouldBeUuid
} = require('validation');

const rule = {
  userId: [
    [shouldBeUuid, 'userId should be valid!']
  ]
};

module.exports.validate = async data => validate(rule, data);
