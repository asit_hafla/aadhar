const Models = require('models');

module.exports = class GetAadharQuery {
  constructor(userId) {
    this.details = {
      userId
    };
  }
  async get() {
    let aadhar = await Models.aadhar.findOne({ where: { userId: this.details.userId } })
    return aadhar;
  }
};
