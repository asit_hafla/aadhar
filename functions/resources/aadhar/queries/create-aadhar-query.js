const Models = require('models');

module.exports = class CreateAadharQuery {
	constructor(id, aadhar, userId) {
		this.details = {
			id,
			aadhar,
			userId
		};
	}
	async get() {
		console.log(this.details)
		const newAadhar = await Models.aadhar.create({
			id: this.details.id,
			aadhar: this.details.aadhar,
			userId: this.details.userId
		})

    return newAadhar;
	}
};
