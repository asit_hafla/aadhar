const Route = require('route');
const { logInfo, respond, whenResult } = require('lib');
const db = require('db/repository');
const GetAadharQuery = require('resources/aadhar/queries/get-aadhar-query.js');
const GetAadharValidation = require('resources/aadhar/validators/get-aadhar-validation');

async function getAadhar(req){
	const { userId } = req.params;


	const validationResult = await GetAadharValidation.validate({ userId });

	const aadhar = await whenResult(
		() => {

			return db.execute(new GetAadharQuery( userId ));

		}
	)(validationResult)

	return respond(aadhar , 'Successfully Fetched Aadhar.','Failed to fetch Aadhar.');
}

Route.withOutSecurity().noAuth().get('/users/:userId/aadhar',getAadhar).bind();
