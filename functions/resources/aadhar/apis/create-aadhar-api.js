const Route = require('route');
const uuid = require('uuid')
const { composeResult, respond } = require('lib');
const db = require('db/repository');
const CreateAadharQuery = require('resources/aadhar/queries/create-aadhar-query.js');
const CreateAadharValidation = require('resources/aadhar/validators/create-aadhar-validation');

async function createAadhar(req){
	const { aadhar } = req.body;
	const { userId } = req.params;
  const id = uuid.v4();

	const newAadhar = await composeResult(
    () => db.execute(new CreateAadharQuery(id, aadhar, userId)),
    (userDetails) => CreateAadharValidation.validate(userDetails)
  )({ id, aadhar, userId })

	return respond(newAadhar , 'Successfully created Aadhar.','Failed to create Aadhar.');
}

Route.withOutSecurity().noAuth().post('/users/:userId/aadhar',createAadhar).bind();
